<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $primaryKey = 'file_id';

    protected $fillable = array('name', 'mime', 'original');

    public function user()
    {
    	return $this->hasMany('App\User');
    }
}
