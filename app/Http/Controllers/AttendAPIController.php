<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Attend;
use Response;

class AttendAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $attend = Attend::all();
        return Response::json(array(
            'error' => false,
            'attends' => $attend->toArray()),
            200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stamps = $request->stamps;
        // dd($stamps);die;
        if (!is_null($stamps)){
            foreach ($stamps['day'] as $stamp) {
                
                $attend = Attend::firstOrCreate(array('user_id' => $stamps['user_id'], 'day' => $stamp));
                $attend->user_id = $stamps['user_id'];
                $attend->day = $stamp;
                $attend->stamp = $stamps['user_id'] . ' ' . $stamp;
                $check = $attend->save();
                // echo $stamp . ' ';
                // echo $stamps['user_id'];
                // \Log::error($stamp['user_id']);
                // \Log::error($stamp['day']);
            }

            if ($check) {
                return Response::json(['result' => true],201);
            } else {
                return Response::json(['result' => false],500);
            }
        // }else {
        //     return Response::json(['result' => 'no data'], 400);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $attend = Attend::find($id);
        if ($attend) {
            return Response::json(array(
                'error' => false,
                'attend' => $attend->toArray()),200);
        }else {
            return Response::json(['result' => 'not found'],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attend = Attend::find($id);

        if (is_null($attend)) {
            return Response::json(['result' => 'not found'],404);
        }

        $check = $attend->delete();

        if (!$check) {
            return Response::json(['result' => false],500);
        }else {
            return Response::json(['result' => true],200);
        }
    }
}
