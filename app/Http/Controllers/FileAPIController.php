<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\File;
use App\User;
use Response;
use Auth;

class FileAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $ip = url();
        
        $files = File::all();
        return Response::json(array(
            'error' => false,
            'path' => $ip . '/images/',
            'file' => $files->toArray()),
            200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()){  
            $id = Auth::user()->id;
            
            if($request->hasFile('image')){
                $file = $request->file('image');
                if($file->isValid()){
                    $extension = $file->getClientOriginalExtension();
                    $name = $file->getFilename() . '.' .$extension;
                    $request->file('image')->move(
                        base_path() . '/public/images/', $name
                    );
                    
                    $upload = new File();
                    $upload->mime = $file->getClientMimeType();
                    $upload->original = $file->getClientOriginalName();
                    $upload->name = $name;
                    $upload->save();

                    $user = User::find($id);
                    $user->file_id = $upload->file_id;
                    $user->picture_name = $name;
                    $user->save();

                    return Response::json('success', 200);
                }
            }else {
                return Response::json('failed', 400);
            }
        } else {
            return Response::json('authentication failed', 505);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $file = File::find($id);
        if ($file) {
            return Response::json(array(
                'error' => false,
                'file' => $file->toArray()),200);
        }else {
            return Response::json('not found',404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
