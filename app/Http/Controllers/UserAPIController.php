<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Response;
use Auth;

class UserAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $path = url() . '/images/';
        $users = User::with('position')->get();
        foreach ($users as $user) {
            $user->picture_name = $path . $user->picture_name;
        }
        if($users) {
            // \Log::error($users);
            return ['users' => $users];
        }else{
            return Response::json(['error' => 'no data'], 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('picture_name');
        if($file->isValid()){
            $extension = $file->getClientOriginalExtension();
            $name = $file->getFilename() . '.' .$extension;
            $request->file('picture_name')->move(
                    base_path() . '/public/images/', $name);
        }
        
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->position_id = $request->position_id;
        $user->gender = $request->gender;
        $user->dateofbirth = $request->dateofbirth;
        $user->address = $request->address;
        $user->file_id = $request->file_id;
        $user->picture_name = $name;
        $check = $user->save();
        if ($check) {
            return Response::json(['result' => true],201);
        } else {
            return Response::json(['result' => false],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $path = url() . '/images/';
        $user = User::with('position')
                ->find($id);
        $user->picture_name = $path . $user->picture_name;
        if ($user) {
            return Response::json(array(
                'error' => false,
                'user' => $user)
            ,200);
        }else {
            return Response::json(['error' => 'not found'],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        // dd($request->name);die;
        // \Log::error($request->name);
        $user->name = $request->name;
        $user->gender = $request->gender;
        $user->dateofbirth = $request->dateofbirth;
        $user->position_id = $request->position_id;
        $user->address = $request->address;
        $check = $user->save();

        if ($check) {
            return Response::json(['result' => true],201);
        } else {
            return Response::json(['result' => false],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            return Response::json(['error '=> 'not found'],404);
        }

        $check = $user->delete();

        if (!$check) {
            return Response::json(['result' => false],500);
        }else {
            return Response::json(['result' => true],200);
        }
    }

    public function postLogin(Request $request) 
    {
        $email = $request->email;
        $password = $request->password;
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
            \Log::info('login',['email' => $email]);
            \Log::info('login', ['password' => $password]);
            \Log::info(Auth::attempt(['email' => $email, 'password' => $password]));
            return Response::json(['result' => true], 200);
            // return redirect()->intended('api/v1/user');
        } else {
            return Response::json([
                'result' => false,
                'error' => 'Not Authorized'
                ], 403);
        }
    }
    public function getLogin() 
    {
        $user = Auth::user();
        return Response::json(['login' => $user->id], 200);
    }

    public function getLogout()
    {
        Auth::logout();
        if(!Auth::check()){
            return Response::json(['result' => true], 200);
        } else {
            return Response::json(['result' => false], 400);
        }
    }
}
