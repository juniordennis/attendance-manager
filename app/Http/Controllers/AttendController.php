<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Attend;
use App\User;
use Input;
use View;
use DB;
use Auth;

class AttendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        if(Auth::user()->position_id == 1){
            $users = User::all();
            return view('attend.index', compact('users', $users));
        } else {
            $id = Auth::user()->id;
            $attends = Attend::where('user_id', '=', $id)->
                orderBy('day', 'asc')->
                get();

            return view('attend.show', compact('attends', $attends));
        } 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stamps = Input::get('stamp');
        if(count($stamps)){
        \Log::error($stamps); 
            foreach ($stamps as $key => $n){
                $inputs = Attend::firstOrCreate(array('user_id' => substr($stamps[$key], 0), 'day' => substr($stamps[$key], -2)));
                $inputs->user_id = substr($stamps[$key], 0);
                $inputs->day = substr($stamps[$key], -2);
                $inputs->stamp = $stamps[$key];
                $inputs->save();
            }
            return redirect('attend')->with('status', 'Data berhasil disubmit!');
        } else {
            return redirect('attend')->with('failed', 'Gagal submit data');

        }

        // $users = DB::table('employees')->get();

        // foreach ($users as $user)
        // {
        //     echo '($user->name)';
        // }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $attends = Attend::where('user_id', '=', $id)->
                orderBy('day', 'asc')->
                get();

        return view('attend.show', compact('attends', $attends));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
