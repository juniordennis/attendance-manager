<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Attend;
use App\User;
use App\Position;
use Response;
use Auth;

class SalaryAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $users = User::all();
        $json = [];
        foreach ($users as $user){
            $totaldays = $user->attend->count();
            $json[] = [
            'id' => $user->id,
            'name' => $user->name,
            'position_id' => $user->position_id,
            // 'days' => $totaldays,
            'salary' => $user->position['salary'] * $totaldays
            ];
        }
        return ['salary' => $json];
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        if($user){
            $position = $user->position;
            $totaldays = $user->attend->count();
            if($user->position_id !== 0){
                return Response::json(array(
                    'error' => false,
                    'salary' => array(
                        'id' => $user->id,
                        'name' => $user->name,
                        'position_id' => $user->position_id,
                        'salary' => $position->salary * $totaldays
                        )
                    ),
                    200);
            }else{
                return Response::json(array(
                    'error' => false,
                    'salary' => array(
                        'id' => $user->id,
                        'name' => $user->name,
                        'position_id' => $user->position_id                    )
                    ),
                    200);
            } 
        } else {
                return Response::json('not found', 404);
            }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
    }
}
