<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Employee;
use App\User;
use Input;
use Auth;
use App\File;
use Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth::user()->position_id == 1){
            $users = User::all();

            return view('user.index', compact('users', $users));
        } else {
            $users = User::find(Auth::user()->id);
            return view('user.index', compact('users', $users));
            // echo $employees;
        }
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newuser = new User;
        $newuser->name = $request->nama_karyawan;
        $newuser->email = $request->email;
        $newuser->password = bcrypt($request->password);
        $newuser->gender = $request->jenis_kelamin;
        $newuser->dateofbirth = $request->tanggal_lahir;
        $newuser->position_id = $request->jabatan;
        $newuser->address = $request->alamat;
        $newuser->save();

        return redirect('user')->with('status', 'Karyawan berhasil ditambah!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return view('user.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('user.edit', compact('user', $user));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $user->name = $request->nama_karyawan;
        $user->gender = $request->jenis_kelamin;
        $user->dateofbirth = $request->tanggal_lahir;
        $user->position_id = $request->jabatan;
        $user->address = $request->alamat;
        $user->save();

        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect()->route('user.index');
    }

    public function up(Request $request,$id)
    {
        $id = ($id == null ? new User() : $id);
       
        $user = User::find($id);
        return view('user.upload', compact('user', $user)); 
    }

    public function upload(Request $request)
    {
        $id = Auth::user()->id;
        if($request->hasFile('image')){
            $file = $request->file('image');
            if($file->isValid()){
                $extension = $file->getClientOriginalExtension();
                $name = $file->getFilename() . '.' .$extension;
                $request->file('image')->move(
                    base_path() . '/public/images/', $name
                );
                
                $upload = new File();
                $upload->mime = $file->getClientMimeType();
                $upload->original = $file->getClientOriginalName();
                $upload->name = $name;
                $upload->save();

                $user = User::find($id);
                $user->file_id = $upload->file_id;
                $user->picture_name = $name;
                $user->save();

                return redirect('user')->with('status', 'Upload berhasil.');
            }
        }else {
            return redirect('up')->with('failed', 'Pilih file yang ingin di upload!');
        }
    }
}
