<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', function() {
    	if (Auth::guest()) {
    		return Redirect::to('auth/login');
    	}elseif (Auth::user()->position_id == 1) {
    		return view('home');
    	}else {
    		$users = App\User::find(Auth::user()->id);
            return view('user.index', compact('users', $users));
    	}	
});


Route::resource('user', 'UserController');
Route::resource('attend', 'AttendController');
Route::resource('salary', 'SalaryController');

Route::get('home', function() {
    $attends = App\Attend::all();
	return view('home', compact('attends', $attends));
});
Route::get('up/{id}', ['as' => 'up', 'uses' => 'UserController@up']);
Route::post('up/upload', ['as' => 'upload', 'uses' =>'UserController@upload']);

Route::get('test', function() {
	$attends = App\Attend::orderBy('user_id', 'asc')->
	orderBy('day', 'asc')->
	get();
	return view('test', compact('attends', $attends));
});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');


// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::get('api/user', ['uses' => 'UserController@index','middleware'=>'simpleauth']);
Route::post('api/user', ['uses' => 'UserController@store','middleware'=>'simpleauth']);

Route::post('api/v1/login', 'UserAPIController@postLogin');
Route::get('api/v1/login', 'UserAPIController@getLogin');
Route::get('api/v1/logout', 'UserAPIController@getLogout');

Route::group(array('prefix' => 'api/v1/', 'middleware' => 'simpleauth'), function()
{
    Route::resource('user', 'UserAPIController');
    Route::resource('attend', 'AttendAPIController');
    Route::resource('salary', 'SalaryAPIController');
    Route::resource('file', 'FileAPIController');
    Route::resource('position', 'PositionAPIController',
        ['only' => 'index']);
});