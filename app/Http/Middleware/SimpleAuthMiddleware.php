<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Response;
class SimpleAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            return $next($request);
        } else {
            return Response::json(['error' => 'Not Authorized'], 403);
        }
    }
}
