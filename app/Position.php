<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table = 'positions';

    protected $primaryKey = 'position_id';

    protected $fillable = array('position', 'salary');


    public function user()
    {
    	return $this->hasMany('App\User');
    }

}
