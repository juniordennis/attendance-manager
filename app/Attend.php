<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Attend extends Model
{
    protected $table = 'attends';

    protected $primaryKey = 'attend_id';

    protected $fillable = array('user_id', 'day', 'stamp');


    public function user()
    {
    	return $this->belongsTo('App\User');

    }

}
