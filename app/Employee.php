<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';

    protected $primaryKey = 'employee_id';

    // protected $dates = 'tanggal_lahir';

    public function position()
    {
    	return $this->belongsTo('App\Position');
    }

    public function attend()
    {
    	return $this->hasMany('App\Attend');
    }
}
