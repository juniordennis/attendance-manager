@extends('layouts.app')

@section('content')
<div class="container" style="width:30%;">
    <!-- resources/views/auth/login.blade.php -->
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="panel panel-info">
        <div class="panel-heading">
            <h2 class="text-center">Register</h2>
            <div class="panel-body">
        <!-- resources/views/auth/register.blade.php -->

                <form method="POST" action="/auth/register">
                    {!! csrf_field() !!}

                    <div>
                        Name
                        <input type="text" name="name" value="{{ old('name') }}" class="form-control">
                    </div>

                    <div>
                        Email
                        <input type="email" name="email" value="{{ old('email') }}" class="form-control">
                    </div>

                    <div>
                        Password
                        <input type="password" name="password" class="form-control">
                    </div>

                    <div>
                        Confirm Password
                        <input type="password" name="password_confirmation" class="form-control">
                    </div>
                    <div>
                        Gender
                        <select name="gender" class="form-control">
                            <option value="Pria">Pria</option>
                            <option value="Wanita">Wanita</option>
                        </select>
                    </div>
                    <div>
                        Date of Birth
                        <input type="date" name="dob" class="form-control">
                    </div>
                    <div>
                        Address
                        <textarea name="address" class="form-control"></textarea>
                    </div>
                    <hr>

                    <div class="text-center">
                        <button type="submit" class="btn btn-info">Register</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop