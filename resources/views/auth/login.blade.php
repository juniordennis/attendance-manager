@extends('layouts.app')

@section('content')

<div class="container" style="width:30%;">
    <!-- resources/views/auth/login.blade.php -->
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h2 class="text-center">Sign in</h2>
            <hr>
            <div class="panel-body">
                <form method="POST" action="/auth/login">
                    {!! csrf_field() !!}

                    <div>
                        Email
                        <input type="email" name="email" value="{{ old('email') }}" class="form-control">
                    </div>

                    <div>
                        Password
                        <input type="password" name="password" id="password" class="form-control">
                    </div>

                    <div>
                        <label class="checkbox-inline"><input type="checkbox" name="remember" class="checkbox"> Remember Me</label>
                    </div>

                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Login</button>
                    </div>
                </form>
            </div>
        </div>
    
</div>


@stop