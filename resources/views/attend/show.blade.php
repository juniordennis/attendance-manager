@extends('layouts.app')

@section('content')
	<div class="container" style="margin-top:20px;">
			
		<table class="table table-hover">
		
			@if(count($attends))	
				@foreach($attends as $attend)
				@endforeach
				<tr>
					<td class="success" style="width:10%;">Name : {{ $attend->user->name }} </td>
				</tr>
				<tr>	
					<th>Day</th>

					@foreach($attends as $attend)
						<td>{{ $attend->day }}</td>
					@endforeach
					<td style="width:10%;" class="info text-right">Total {{ App\Attend::where('user_id', $attend->user_id)->count() }} days</td>
				</tr>
			@else
				<div class="container">
					<label class="label label-info text-center">no data yet.</label>
				</div>	
				
			@endif
		</table>
	</div>	
@stop
