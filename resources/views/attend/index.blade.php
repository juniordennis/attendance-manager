@extends('layouts.app')

@section('content')

<div class="container" style="margin-top:20px">
	<div>
		@if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
	   	@elseif (session('failed'))
	    <div class="alert alert-warning">
	        {{ session('failed') }}
	    </div>
		@endif

		{!!Form::open(['route' => 'attend.store'])!!}

		<div>
			@if(Auth::user()->position_id == 1)
				@if(count($users))
					<table class="table table-condensed">
						<tr>
							<th>ID</th>
							<th>Name</th>
							
							<?php $d=cal_days_in_month(CAL_GREGORIAN,11,2015) ?>
							
							@for ($i=1; $i<=$d; $i++)
								<th>{{ $i }}</th>
							@endfor
							
						</tr>
						
						@foreach($users as $user)
							<tr>
								<td>{{ $user->id }}</td>
								<td><a href="{{route('attend.show', $user->id)}}">{{$user->name}}</a></td>
								
								
								@for ($i=1; $i<=$d; $i++)
									<td>
										<!-- {!! Form::text('day', $user->user_id . '-' . $i) !!} -->

										{!! Form::checkbox('stamp[]', $user->id . ' ' . $i, false, ['id' => 'stamp' . $user->id . '-' . $i]) !!}
									</td>
								@endfor

							</tr>
						@endforeach

					</table>
				</div>
				@endif
			@else
				<table class="table table-hover">
					@if(count($attends))	
						@foreach($attends as $attend)
						@endforeach
						<tr>
							<td class="success" style="width:10%;">Name : {{ $attend->user->name }} </td>
						</tr>
						<tr>	
							<th>Day</th>

							@foreach($attends as $attend)
								<td>{{ $attend->day }}</td>
							@endforeach
							<td style="width:10%;" class="info text-right">Total {{ App\Attend::where('user_id', $attend->user_id)->count() }} days</td>
						</tr>
					@else
						<div class="container">
							<label class="label label-info text-center">no data yet.</label>
						</div>	
						
					@endif
				</table>

			@endif

		<div class="text-right">
			{!!Form::submit('Submit', ['class' => 'btn btn-primary'])!!}
		</div>

		{!!Form::close()!!}
	</div>
	

</div>


@stop