<!DOCTYPE html>
<html lang="en">
    <head>
        @include('includes.head')

        <!-- CSS And JavaScript -->
    </head>

    <body>
        <div class="container">
            <nav role="navigation" class="navbar navbar-inverse">
                <div class="navbar-header">
                    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="#" class="navbar-brand" id="main-title">Laravel 5</a>
                </div>
                <!-- Navbar Contents -->
                <div id="navbarCollapse" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right" id="nav-up">
                        @if (Auth::check())
                            <li class="active"><a>Welcome {{ Auth::user()->name }}</a></li>

                            <li><a href="{{URL::to('/home')}}"></i> Home</a></li>
                            @if (Auth::user()->position_id == 1)
                                <li><a href="{{URL::to('/user')}}"></i> Employee</a></li>
                            @else
                                <li><a href="{{URL::to('/user')}}"></i> Profil</a></li>
                            @endif
                            <li><a href="{{URL::to('/attend')}}"></i> Attendance</a></li> 
                            <li><a href="{{URL::to('/salary')}}"></i> Salary</a></li> 
                            <li><a href="{{URL::to('auth/logout')}}">Log out <span class="glyphicon glyphicon-log-out"></span></a></li>
                        @else 
                            <li><a href="{{URL::to('auth/register')}}"></i> Register</a></li>
                            <li><a href="{{URL::to('auth/login')}}">Login <span class="glyphicon glyphicon-log-in"></span></a></li>
                        @endif
                    </ul>
                </div>
            </nav>
        </div>
        <div>
            @yield('content')
        </div>

        <footer>
            @include('includes.footer')
        </footer>
    </body>
</html>