@extends('layouts.app')

@section('content')

<div class="container">
	<div>
		@if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
		@endif		
		<div class="container" style="width:30%;">
		@if(Auth::user()->position_id == 1)
			<div class="text-center">
				<a class="btn btn-success" href="{{route('user.create')}}" style="margin-bottom:20px; text-decoration:none;">Create</a>
			</div>

			@if(count($users))
				<table class="table table-condensed">
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Position</th>
						
					</tr>
					
					@foreach($users as $user)
						<tr>
							<td>{{ $user->id }}</td>
							<td><a href="{{route('user.show', $user->id)}}">{{ $user->name }}</a></td>
							<td> {{ $user->position['position'] }} </td>
						</tr>
					@endforeach

				</table>
			</div>		
			@endif
		@else
			@if(count($users))
				<table class="table table-condensed">
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Position</th>
					</tr>
					<tr>
						<td> {{ $users->id }} </td>
						<td><a href="{{route('user.show', $users->id)}}"> {{ $users->name }} </a></td>
						<td> {{ $users->position['position'] }} </td>
					</tr>
				</table>
			@endif
		@endif
	</div>
</div>


@stop
<!-- <td><a href=""><span class="glyphicon glyphicon-ok"></span></a></td>
<td><a href=""><span class="glyphicon glyphicon-minus"></span></a></td> -->