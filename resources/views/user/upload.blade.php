@extends('layouts.app')

@section('content')
<div class="container text-center">
	<div class="container" style="width:30%;">
		@if (session('status'))
		    <div class="alert alert-success">
		        {{ session('status') }}
		    </div>
		@elseif (session('failed'))
			<div class="alert alert-danger">
		        {{ session('failed') }}
		    </div>
		@endif

		<form action="{{route('upload')}}" method="post" enctype="multipart/form-data">
	        <input type="file" name="image" class="form-control" 
	        	accept="image/gif, image/jpeg, image/png, image/bitmap" required>
	        <input type="hidden" name="_token" value="{{ csrf_token() }}">
	        <hr>
	        <input type="submit" value="Upload" class="btn btn-primary">
	    </form>
	</div>
</div>

@stop