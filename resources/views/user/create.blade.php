@extends('layouts.app')

@section('content')
<div class="container" style="width:30%;">	
	{!!Form::open(['route' => 'user.store'])!!}

	{!!Form::label('nama', 'Nama Karyawan')!!}
	{!!Form::text('nama_karyawan', null, array('class' => 'form-control', 'required'))!!}

	{!!Form::label('email', 'Email')!!}
	{!!Form::email('email', null, array('class' => 'form-control', 'required'))!!}

	{!!Form::label('password', 'Password')!!}
	{!!Form::password('password', array('class' => 'form-control', 'required'))!!}
	
	{!!Form::label('jeniskelamin', 'Jenis Kelamin')!!}
	{!!Form::select('jenis_kelamin', array('Pria' => 'Pria', 'Wanita' => 'Wanita'), 'Pria', ['class' => 'form-control'] )!!}

	{!!Form::label('tanggal', 'Tanggal Lahir')!!}
	<!-- {!!Form::text('tanggal_lahir')!!} -->
	<input type="date" name="tanggal_lahir" class="form-control"></input>
	
	<!-- {!! Form::macro('custom', function() {
	    return '<input type="custom">';
	}); !!} -->
	{!!Form::label('jabatan', 'Jabatan')!!}
	{!!Form::select('jabatan', array('1' => 'HRD', 'Technical Staff', '3' => 'Project Handler'), '1',['class' => 'form-control'] ) !!}

	{!!Form::label('alamat', 'Alamat')!!}
	{!!Form::textarea('alamat', null, ['class' => 'form-control'])!!}
	<hr>
	{!!Form::submit('Create', ['class' => 'btn btn-success btn-block'])!!}

	{!!Form::close()!!}
</div>
@stop