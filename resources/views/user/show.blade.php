@extends('layouts.app')

@section('content')
	{!!Form::open([
		'method' => 'delete',
		'route' => ['user.destroy', $user->id]
	])!!}	
	<div class="container" style="margin-top:20px; width:50%;">	
		<table class="table table-hover">
			<tr class="text-center">
				<td colspan="3">
					<div style="height:200px;">
					<?php $image = $user->picture_name; ?>

					<img src="{{ URL::asset('images/'. $image) }}" style="max-width:100%; max-height:100%;"></img>
					</div>
				</td>
			</tr>
			<tr class="text-center">
				<td colspan="3"><a href="{{route('up', $user->id)}}" class="btn btn-primary btn-sm">Upload Image</a></td>
			</tr>
			<tr>
				<td class="success" style="width:25%;">
					ID 
				</td>
				<td>
					{{$user->id}}
				</td>
			</tr>
			<tr>
				<td class="info">
					Name 
				</td>
				<td>
					{{$user->name}}
				</td>
			</tr>
			<tr>
				<td class="info">
					Gender
				</td>
				<td>
					{{$user->gender}}
				</td>
			</tr>
			<tr>
				<td class="info">
					Date of Birth
				</td>
				<td>
					{{$user->dateofbirth}}
				</td>
			</tr>
			<tr>
				<td class="info">
					Address
				</td>
				<td>
					{{$user->address}}
				</td>
			</tr>
			<tr>
				<td colspan="2" class="danger text-center">
					<a href="{{route('user.edit', $user->id)}}" class="btn btn-primary btn-sm">Edit</a>
				@if (Auth::user()->position_id == 1)
					{!!Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
				@endif
				</td>
			</tr>
		</table>
	</div>
	{!!Form::close()!!}
@stop