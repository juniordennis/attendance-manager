@extends('layouts.app')

@section('content')
<div class="container" style="width:30%;">
	{!!Form::model($user, [
		'method' => 'patch',
		'route' => ['user.update', $user->id]
	])!!}

	{!!Form::label('nama', 'Nama Karyawan')!!}
	{!!Form::text('nama_karyawan', $user->name, ['class' => 'form-control'])!!}
	
	{!!Form::label('jenis', 'Jenis Kelamin')!!}
	{!!Form::select('jenis_kelamin', array('Pria' => 'Pria', 'Wanita' => 'Wanita'), $user->gender, ['class' => 'form-control'])!!}

	{!!Form::label('tanggal', 'Tanggal Lahir')!!}
	<!-- {!!Form::text('tanggal_lahir', $user->dateofbirth, ['class' => 'form-control'])!!} -->
	<input type="date" name="tanggal_lahir" class="form-control" value="{{$user->dateofbirth}}"></input>
	@if (Auth::user()->position_id == 1)
		{!!Form::label('jabatan', 'Jabatan')!!}
		{!!Form::select('jabatan', array('1' => 'HRD', 'Technical Staff', '3' => 'Project Handler'), $user->position_id,['class' => 'form-control'] ) !!}
	@endif

	{!!Form::label('alamat', 'Alamat')!!}
	{!!Form::textarea('alamat', $user->address, ['class' => 'form-control'])!!}
	<hr>	
	{!!Form::submit('Update', ['class' => 'btn btn-primary btn-block'])!!}
				
	{!!Form::close()!!}

</div>
@stop