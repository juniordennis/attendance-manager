<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>  
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><i class='fa fa-linux'>WELCOME TO </i> LARAVEL 5 </a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="{{URL::to('/home')}}"></i> Home</a></li>
        <li><a href="{{URL::to('/stock')}}"></i> Stock</a></li>
        <li><a href="{{URL::to('/about')}}"></i> About</a></li>
        <li><a href="{{URL::to('auth/register')}}"></i> Register</a></li>
        <li><a href="{{URL::to('auth/login')}}"></i> Login</a></li>
        <li><a href="{{URL::to('auth/logout')}}"></i> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>