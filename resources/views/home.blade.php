@extends('layouts.app')

@section('content')


    <div class="container text-center">
        <h2>Total Daily Employee Attendance</h2> <hr>
        <canvas id="kursSG" width="1100" height="400"></canvas>
        <?php $d=cal_days_in_month(CAL_GREGORIAN,11,2015) ?>
        <script>

			var lineData = {
				labels : [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],
				datasets : [
					{
						fillColor : "rgba(172,194,132,0.1)",
						strokeColor : "#48A4D1",
						pointColor : "#fff",
						pointStrokeColor : "#9DB86D",
						data : [
							{{ $days = App\Attend::where('day', 1)->count() }},
							{{ $days = App\Attend::where('day', 2)->count() }},
							{{ $days = App\Attend::where('day', 3)->count() }},
							{{ $days = App\Attend::where('day', 4)->count() }},
							{{ $days = App\Attend::where('day', 5)->count() }},
							{{ $days = App\Attend::where('day', 6)->count() }},
							{{ $days = App\Attend::where('day', 7)->count() }},
							{{ $days = App\Attend::where('day', 8)->count() }},
							{{ $days = App\Attend::where('day', 9)->count() }},
							{{ $days = App\Attend::where('day', 10)->count() }},
							{{ $days = App\Attend::where('day', 11)->count() }},
							{{ $days = App\Attend::where('day', 12)->count() }},
							{{ $days = App\Attend::where('day', 13)->count() }},
							{{ $days = App\Attend::where('day', 14)->count() }},
							{{ $days = App\Attend::where('day', 15)->count() }},
							{{ $days = App\Attend::where('day', 16)->count() }},
							{{ $days = App\Attend::where('day', 17)->count() }},
							{{ $days = App\Attend::where('day', 18)->count() }},
							{{ $days = App\Attend::where('day', 19)->count() }},
							{{ $days = App\Attend::where('day', 20)->count() }},
							{{ $days = App\Attend::where('day', 21)->count() }},
							{{ $days = App\Attend::where('day', 22)->count() }},
							{{ $days = App\Attend::where('day', 23)->count() }},
							{{ $days = App\Attend::where('day', 24)->count() }},
							{{ $days = App\Attend::where('day', 25)->count() }},
							{{ $days = App\Attend::where('day', 26)->count() }},
							{{ $days = App\Attend::where('day', 27)->count() }},
							{{ $days = App\Attend::where('day', 28)->count() }},
							{{ $days = App\Attend::where('day', 29)->count() }},
							{{ $days = App\Attend::where('day', 30)->count() }}
							
						]
					}

				]
			}
			Chart.defaults.global.responsive = true;
			var kurs = document.getElementById('kursSG').getContext('2d');
			new Chart(kurs).Line(lineData);
        	
		</script>
    </div>
@endsection