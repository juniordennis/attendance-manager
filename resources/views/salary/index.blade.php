@extends('layouts.app')

@section('content')
	<div class="container">
		<h2 class="text-center">Employee Salary</h2><hr>
		<!-- <h1 class="text-center">Under Maintenances</h1> <hr> -->
	</div>
<div class="container" style="width:50%; margin-top:20px;">
	<table class="table table-hover">
		<tr>
			<th>
				Name
			</th>
			<th>
				Days
			</th>
			<th class="text-right">
				Salary
			</th>
		</tr>
		@if(Auth::user()->position_id == 1)
			@foreach($users as $user)
				<tr>
					<td> {{ $user->name }} </td>
					<td> {{ $totaldays = App\Attend::where('user_id', $user->id)->count() }}</td>
					<td class="text-right"> Rp. {{ $salary = $user->position['salary'] * $totaldays }} </td>
				</tr>
			@endforeach
		@else
			<tr>
				<td> {{ $users->name }} </td>
				<td> {{ $totaldays = App\Attend::where('user_id', $users->id)->count() }} </td>
				<td class="text-right"> Rp. {{ $salary = $users->position['salary'] * $totaldays }} </td>
			</tr>
		@endif

	</table>
</div>

@stop